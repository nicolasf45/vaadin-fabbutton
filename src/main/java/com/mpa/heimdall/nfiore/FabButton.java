/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpa.heimdall.nfiore;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.templatemodel.TemplateModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author nicolas
 */
@Tag("fab-button")
@HtmlImport("frontend://bower_components/fab-button/fab-button.html")
@HtmlImport("frontend://bower_components/fab-button/fab-button-sizing.html")

public class FabButton extends PolymerTemplate<TemplateModel> implements HasStyle {
//    private @Id("button") Button button;

    private @Id("ulSubItems") UnorderedList ul;
    private @Id("left-buttons") UnorderedList leftItemUl;
    private @Id("divFab") Div containerFabDiv;
    private @Id("span") Span cuerpoButtonSpan;
    private @Id("fab-left-button") ListItem leftFabListItem;
    private @Id("left-button-container") Anchor leftButtonContainer;

    private List<FabButtonItem> subItems;
    private Icon icon;

    private int zIndex;

    private Registration leftItemRegistration;
    private Registration clickItemRegistration;

    public enum size {
        SMALL,
        MEDIUM,
        LARGE
    }

    public FabButton(Icon i) {
        this.icon = i;
        this.cuerpoButtonSpan.add(this.icon);
        init();
    }

    public FabButton(Icon icon, FabButtonItem... items) {
        subItems = Arrays.asList(items);
        for (FabButtonItem item : subItems) {
            this.ul.add(items);
        }
    }

    private void init() {
        this.subItems = new ArrayList<FabButtonItem>();
        this.icon.addClassName("fab-action-button-icon");
    }

    public void setSubItems(FabButtonItem... fabButtonItems) {
        this.subItems = Arrays.asList(fabButtonItems);
        this.ul.removeAll();
        for (FabButtonItem items : this.subItems) {
            this.ul.add(items);
        }
    }

    public void addSubItem(FabButtonItem item) {
        this.subItems.add(item);//agrego a la lista
        this.ul.add(item);//muestro en pantalla 
    }

    /**
     * Agrega un listener al cuerpo del boton, que se ejecuta luego de detectar un click
     * @param listener
     *          listener a ser agregado
     * @return handle que puede ser usado para remover el listener
     */
    public void addClickListener(ComponentEventListener<ClickEvent<Span>> listener) {
        this.clickItemRegistration = this.cuerpoButtonSpan.addClickListener(listener);
    }

    /**
     * Agrega un listener, al cuerpo del boton, que se ejecuta luego de detectar un click
     * @param listener
     *          listener a ser agregado
     * @return handle que puede ser usado para remover el listener
     */
    public void addLeftItemClickListener(ComponentEventListener<ClickEvent<ListItem>> listener) {
        this.leftItemRegistration = this.leftFabListItem.addClickListener(listener);
    }

    @EventHandler
    private void hoverLeave() {
        for (FabButtonItem item : subItems) {
            item.showTooltip(false);
        }
    }

    @EventHandler
    private void hoverEnter() {
        for (FabButtonItem item : subItems) {
            item.showTooltip(true);
        }
    }

    /**
     * Agrega o remueve estilos para que se muestre el cursor con estilo "pointer" al hacer hover
     * @param b
     */
    public void setClickeable(Boolean b) {
        if (b) {
            this.cuerpoButtonSpan.getStyle().set("cursor", "pointer");
        } else {
            this.cuerpoButtonSpan.getStyle().remove("cursor");
        }
    }


    public void setSize(FabButton.size size) {
        //se remueve tamaño por 
        this.containerFabDiv.removeClassName("fab-l");
        this.containerFabDiv.removeClassName("fab-m");
        this.containerFabDiv.removeClassName("fab-s");
        switch (size) {
            case SMALL:
                this.containerFabDiv.addClassName("fab-s");
                break;
            case MEDIUM:
                this.containerFabDiv.addClassName("fab-m");
                break;
            case LARGE:
                this.containerFabDiv.addClassName("fab-l");
                break;
        }
        for (FabButtonItem item:subItems){
            item.setSize(size);
        }
    }

    public void setBottomRightPosition(int bottom, int right){
        this.containerFabDiv.getStyle()
                .set("bottom" , String.valueOf(bottom))
                .set("right" , String.valueOf(right));
    }

    public void setZindex(int i) {
        this.zIndex = i;
        this.containerFabDiv.getStyle().set("zindex", String.valueOf(i));
    }

    /**
     * Determina la distancia desde el "piso"
     * @param distancia
     *          string que puede ser interpretado por css
     */
    public void setBottomPosition(String distancia) {
        this.containerFabDiv.getStyle().set("bottom" , distancia);
    }

    private Icon initIconStyle(Icon icon) {
        icon.addClassName("fab-action-button-icon");
        //        icon.addClassName("");
        return icon;
    }

    public void setLeftItemIcon(Icon icon){
        leftButtonContainer.removeAll();
        icon.addClassName("fab-left-buttons__icon");
        leftButtonContainer.add(icon);
    }

    /**
     * Agrega un atajo que dispara "click" en el componente
     * @param key
     *          tecla que dispara el evento click
     */
    public void addClickShortcut(Key key){
        this.cuerpoButtonSpan.addClickShortcut(key);
    }


    /**
     *  Agrega estilos a FabButton para que el item izquierdo este siempre visible.
     */
    public void setLeftItemVisible(){
        this.containerFabDiv.addClassName("force_hover");
    }

    public void setLeftItemOccult(){
        this.containerFabDiv.addClassName("force_occult");
    }

    /**
     *
     * @param color
     *          Valor del color interpretable por css
     */
    public void setColor(String color) {
        this.cuerpoButtonSpan.getStyle().set("background-color", color);
    }

    /**
     * Permite cambiar los iconos de los botones y tambien sus handlers/events
     */
    public void invertButtons(){

    }


}
