/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpa.heimdall.nfiore;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.shared.Registration;


/**
 *
 * @author nicolas
 */
@Tag("fab-button-item")
@HtmlImport("frontend://bower_components/fab-button/fab-button-item.html")
public class FabButtonItem extends PolymerTemplate<FabButtonItemModel> {
    private @Id("item")
    ListItem li;
    private @Id("anchor")
    Anchor a;

    private Icon icon;
    private String tooltip; 
    
    private int size; 
    
//    private List<ClickEvent> listaListeners;
 
    public FabButtonItem(){
        
    }
    
    public FabButtonItem(Icon icon){
        this.icon = icon;
        this.icon.getStyle().set("align-self" , "center");
        this.icon.addClassName("fab-button-item-icon");
        this.a.add(icon);
    }
    
    public FabButtonItem(String s){
        this.tooltip = s; 
        getModel().setTooltipProperty(s);
    }
    
    public FabButtonItem(Icon icon, String tooltip){
        this.icon = icon;
        this.icon.getStyle().set("align-self" , "center");
        this.icon.addClassName("fab-button-item-icon");
        this.tooltip = tooltip; 
        getModel().setTooltipProperty(tooltip);
        this.a.add(icon);
    }
        
    public Registration addItemClickListener(ComponentEventListener<FabItemClick> listener) {
        return addListener(FabItemClick.class, listener);
    }
    
    @DomEvent("click")
    static public class FabItemClick extends ComponentEvent<FabButtonItem> {
        private FabButtonItem fabButtonItem; 
        private String event;
        public FabItemClick(FabButtonItem source,boolean fromClient, @EventData("event.srcElement.tagName") String type){
            super(source, fromClient);
            this.event = type;
            this.fabButtonItem = source;
//            source.nodos.stream().filter(nodo->nodo.getId().equals(id)).findFirst().ifPresent(founditem->this.nodo=founditem);
        }
        
        public FabButtonItem getFabButtonItem(){
            return this.fabButtonItem;
        }
        
        public String getEvent(){
            return this.event;
        }
    }
    
    public String getTooltip() {
        return this.tooltip;
    }
    
    public void setSize(FabButton.size size){
        switch(size){
            case SMALL:
                this.li.addClassName("fab-s");
                this.a.addClassName("fab-s");
                this.icon.addClassName("fab-s");
                break;
            case MEDIUM:
                this.li.addClassName("fab-m");
                this.a.addClassName("fab-m");
                this.icon.addClassName("fab-m");
                break;
            case LARGE:
                this.li.addClassName("fab-l");
                this.a.addClassName("fab-l");
                this.icon.addClassName("fab-l");
                break;                
        }
    }
    
    public void showTooltip(Boolean b){
        if(b){
            this.a.addClassName("tooltip_visible");
        }else{
            this.a.removeClassName("tooltip_visible");
        }
    }
    
//      @ClientCallable
//      public void Click(){
//        for (ClickEvent el : this.listaListeners) {
//            
//        }
//      }
//      
//       public void addEventListener(ClickEvent eventListener) {
//            if (this.listaListeners == null) {
//                this.listaListeners = new ArrayList<>();
//            }
//            this.listaListeners.add(eventListener);
//        }
    
}
