/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpa.heimdall.nfiore;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 *
 * @author nicolas
 */
public interface FabButtonItemModel extends TemplateModel {

    void setTooltipProperty(String tooltipValue);

    String getTooltipProperty();
}